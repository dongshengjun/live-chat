# 点赞（star）   
赞多了我才有动力继续开发功能。  
***

# 项目说明
nodejs聊天室，redis存储，sokect通信。  
***   

# 实现功能
0:弹幕（所有人可见）。

1:在线用户实时推送。

2:实时聊天。

3:提示消息被阅读。

4:显示未读消息数量。

5:注册登录。
***

# 在线演示地址
http://47.94.2.0/
***
# 注册密钥:
xjbmy
***
# 如何聊天
注册之后点击右上角当前登录用户就能聊天
***


![演示](http://chuantu.biz/t6/13/1503210182x2890149655.png "在这里输入图片标题")

#  如何在本地启动  

将frontend中的store.js的 host: '47.94.2.0' 改成host: '127.0.0.1',然后进行以下操作即可  

dev模式 npm run dev 然后启动server.js，访问localhost:8081  

prod模式 npm run build 然后启动server.js，访问localhost:8080  

***
# License
[MIT](https://opensource.org/licenses/MIT)

Copyright (c) 2017-present,  SunZhengJie(Furioussoulk)
